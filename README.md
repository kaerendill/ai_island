Ce projet a pour but de générer une île puis de mettre une IA dessus pour qu'elle apprenne à survivre dans le but de s'échapper.

J'ai récupéré l'algorithme de Fortune du lien suivant que j'ai légèrement modifié : https://github.com/gorhill/Javascript-Voronoi

Pour le bruit de Perlin j'ai utilisé la page Wikipédia : https://fr.wikipedia.org/wiki/Bruit_de_Perlin. Mais ce dernier reste à améliorer car il ne corespond pas tout à fait à ce que je désire pour la forme de l'île.
