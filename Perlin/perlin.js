const gradients = [{x:1,y:0},{x:0,y:1},{x:1,y:1},{x:-1,y:0},{x:0,y:-1},{x:-1,y:-1},{x:-1,y:1},{x:1,y:-1}];

function perlin(diagram){
    diagram.cells.forEach(cell=>{
        const nx = cell.site.x/size - 0.5;
        const ny = cell.site.y/size - 0.5;
        const d = Math.sqrt(nx**2 + ny**2) / Math.sqrt(0.5);
        const e = multiOctavePerlinNoise(cell.site.x, cell.site.y, 13);
        cell.site.perlin =Math.pow((1+e-d)/2,0.5);
        cell.site.type = cell.site.perlin < 0.5 ? "water" : "dirt";
        checkType(cell);
        if(cell.site.type == "dirt"){
            dirts.push(cell);
        }else{
            waters.push(cell);
        }
    });
}

function checkType(cell){
    cell.halfedges.forEach(halfedge=>{
        if(!halfedge.edge.r_site || !halfedge.edge.l_site){
            cell.site.type = "water";
        }
    });
}

function lerp(a0, a1, w){
    return (1 - w)*a0 + w*a1;
}

function dotGridGradient(ix, iy, x, y){
    const dx = x - ix;
    const dy = y - iy;
    const i = getRandomIntInclusive(0, gradients.length-1);
    
    return (dx*gradients[i].x + dy*gradients[i].y);
}

function perlinNoise(x, y){
    const x0 = Math.floor(x);
    const x1 = x0 + 1;
    const y0 = Math.floor(y);
    const y1 = y0 + 1;

    const s_x = x - x0;
    const s_y = y - y0;

    let n0 = dotGridGradient(x0, y0, x, y);
    let n1 = dotGridGradient(x1, y0, x, y);
    const i_x0 = lerp(n0, n1, s_x);
    n0 = dotGridGradient(x0, y1, x, y);
    n1 = dotGridGradient(x1, y1, x, y);
    const i_x1 = lerp(n0, n1, s_x);
    const value = lerp(i_x0, i_x1, s_y);

    return value;
}

function multiOctavePerlinNoise(x, y, octaves){
    let v = 0;
    let scale = 1;
    let weight = 1;
    let weight_total = 0;
    for(let i = 0; i < octaves; i++){
        v += perlinNoise(x * scale, y * scale) * weight;
        weight_total += weight;
        scale *= 0.5; 
        weight *= 2.0;
    }
    return v / weight_total;
}