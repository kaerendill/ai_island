const canvas = document.querySelector("canvas");
const ctx = canvas.getContext("2d");
const size = 1024;
const bbox = {
    xl : 0,
    xr : size,
    yt : 0,
    yb : size
};
const sites = [];
const dirts = [];
const lakes = [];
const waters = [];
const mountains = [];
const to_splice = [];

function gensites(){
    for(let i=0; i<5000; i++){
        const site = new Site(getRandomIntInclusive(0, size-1), getRandomIntInclusive(0, size-1));
        sites.push(site);
    }
    sites.sort((a,b)=>{return a.x == b.x ? a.y-b.y : a.x-b.x});
}

gensites();
const voronoi = new Voronoi(sites, size, bbox);
perlin(voronoi.diagram);
shapeIsland(voronoi.diagram, ctx);
console.log(lakes);
console.log(dirts);
console.log(voronoi.diagram.cells);

function shapeIsland(diagram, ctx){
    flood(diagram.cells[0], ctx);
    waters.forEach(water=>{
        let nb_dirts = 0;
        water.neighbors.forEach(neighbor=>{
            if(neighbor.site.type == "beach"){
                water.site.type="dirt";
                nb_dirts++;
            }
            if(neighbor.site.type == "dirt"){
                nb_dirts++;
            }
        });
        if(water.site.type == "water" && nb_dirts < water.neighbors.length){
            water.site.type = "lake";
            lakes.push(water);
        }else{
            water.site.type = "dirt";
            dirts.push(water);
        }
    });
    waters.length=0;
    increaseLacksSize();
    let first_dirt_neighbor = undefined;
    let i = 0;
    while(!first_dirt_neighbor){
        first_dirt_neighbor = lakes[i].neighbors.find(neighbor=>neighbor.site.type == "dirt");
        i++;
    }
    moisureFlood(first_dirt_neighbor, lakes[i]);
    checkDirts();
    increaseMountainSize();
    diagram.cells.forEach(cell=>{
        drawCell(cell, ctx);
    });
}

function flood(cell, ctx){
    cell.site.type = "ocean";
    waters.splice(waters.findIndex(water=>water.site.voronoi_id == cell.site.voronoi_id), 1);
    cell.site.perlin = 0;
    cell.neighbors.forEach(neighbor=>{
        if(neighbor.site.type == "water"){
            flood(neighbor, ctx);
        }else if(neighbor.site.type == "dirt"){
            let nb_water = 0;
            neighbor.neighbors.forEach(neighbor_of_neighbor=>{
                if(neighbor_of_neighbor.site.type == "water" || neighbor_of_neighbor.site.type == "ocean"){
                    nb_water++;
                }
            });
            if(nb_water >= neighbor.neighbors.length-1){
                neighbor.site.type = "ocean";
            }else{
                neighbor.site.type = "beach";
            }            
            dirts.splice(dirts.findIndex(dirt=>dirt.site.voronoi_id == neighbor.site.voronoi_id), 1);
        }
    });
}

function increaseLacksSize(){
    lakes.forEach(lake=>{
        let cell_to_dirt = true;
        let neighbor_aside_beach = false;
        let neighbor_to_lake = false;
        lake.neighbors.forEach(neighbor=>{
            if(neighbor.site.type == "dirt"){
                neighbor.neighbors.forEach(neighbor_of_neighbor=>{
                    if(neighbor_of_neighbor.site.type == "lake" && neighbor_of_neighbor.site.voronoi_id != neighbor.site.voronoi_id && neighbor_of_neighbor.site.voronoi_id != lake.site.voronoi_id){
                        neighbor_to_lake = true;
                    }
                    if(neighbor_of_neighbor.site.type == "beach"){
                        neighbor_aside_beach = true;
                    }
                });
                if(!neighbor_aside_beach && neighbor_to_lake){
                    cell_to_dirt = false;
                    neighbor.site.type = "lake";
                    dirts.splice(dirts.findIndex(dirt => neighbor.site.voronoi_id == dirt.site.voronoi_id),1);
                    lakes.push(neighbor);
                }
            }
        });
        if(cell_to_dirt){
            lake.site.type = "dirt";
            to_splice.push(lake.site.voronoi_id);
            dirts.push(lake);
        }
    });
    to_splice.forEach(i=>{
        lakes.splice(lakes.findIndex(lake => lake.site.voronoi_id == i),1);
    });
    to_splice.length = 0;
}

function moisureFlood(cell, origin){
    const d_center = Math.sqrt((size/2 - cell.site.x)**2 + (size/2 - cell.site.y)**2)/size;
    const d_lake = 1-Math.sqrt((origin.site.x - cell.site.x)**2 + (origin.site.y - cell.site.y)**2)/Math.sqrt((size/2 - origin.site.x)**2 + (size/2 - origin.site.y)**2);
    cell.moisure = (1+(d_center + d_lake/1.2))/2;
    if(cell.moisure >= 0.8 && cell.site.perlin < 0.72){
        cell.site.type = "deep forest";
    }else if(cell.moisure >= 0.5 && cell.site.perlin < 0.72){
        cell.site.type = "forest";
    }else if(cell.moisure >= 0.5 && cell.site.perlin >= 0.72){
        cell.site.type = "mountain";
        mountains.push(cell);
    }else if(cell.site.perlin >= 0.68){
        cell.site.type = "rock";
    }else{
        cell.site.type = "plain";
    }
    dirts.splice(dirts.findIndex(dirt=> dirt.site.voronoi_id == cell.site.voronoi_id),1);
    cell.neighbors.forEach(neighbor=>{
        if(neighbor.site.type == "dirt" && !neighbor.moisure){
            neighbor.neighbors.forEach(neighbor_of_neighbor=>{
                if(neighbor_of_neighbor.site.type == "lake"){
                    origin = neighbor_of_neighbor;
                }
            });
            moisureFlood(neighbor, origin);
        }
    });
}

function checkDirts(){
    dirts.forEach(dirt=>{
        let alone = true;
        let i = 0;
        while(i < dirt.neighbors.length && alone){
            if(dirt.neighbors[i].site.type == "dirt"){
                alone = false;
            }
            i++;
        }
        if(alone){
            dirt.site.type = dirt.neighbors[0].site.type;
        }else{
            dirt.site.type = "beach";
        }
    });
    dirts.length=0;
}

function increaseMountainSize(){
    mountains.forEach(mountain=>{
        let cell_to_dirt = true;
        let neighbor_aside_lake = false;
        let neighbor_to_mountain = false;
        mountain.neighbors.forEach(neighbor=>{
            if(neighbor.site.type != "lake"){
                neighbor.neighbors.forEach(neighbor_of_neighbor=>{
                    if(neighbor_of_neighbor.site.type == "rock" || neighbor_of_neighbor.site.type == "mountain" && neighbor_of_neighbor.site.voronoi_id != neighbor.site.voronoi_id && neighbor_of_neighbor.site.voronoi_id != mountain.site.voronoi_id){
                        neighbor_to_mountain = true;
                    }
                    if(neighbor_of_neighbor.site.type == "lake"){
                        neighbor_aside_lake = true;
                    }
                });
                if(!neighbor_aside_lake && neighbor_to_mountain){
                    cell_to_dirt = false;
                    neighbor.site.type = "mountain";
                    mountains.push(neighbor);
                }
            }
        });
        if(cell_to_dirt){
            mountain.site.type = mountain.neighbors[0].site.type;
            to_splice.push(mountain);
        }
    });
    to_splice.forEach(i=>{
        mountains.splice(mountains.findIndex(elt=>elt.site.voronoi_id == i),1);
    })
}

function drawCell(cell, ctx){
    let color = "red";
    if(cell.site.type == "ocean"){
        color = "20266c"
    }else if(cell.site.type == "dirt"){
        color = "b26951";
    }else if(cell.site.type == "beach"){
        color = "e0cda9";
    }else if(cell.site.type == "lake"){
        color = "50c8e0";
    }else if(cell.site.type == "rock"){
        color = "9ca19f";
    }else if(cell.site.type == "deep forest"){
        color = "444720";
    }else if(cell.site.type == "forest"){
        color = "688852";
    }else if(cell.site.type == "plain"){
        color = "bad562";
    }else if(cell.site.type == "mountain"){
        color = "616568";
    }
    ctx.beginPath();
    ctx.moveTo(cell.halfedges[0].edge.va.x, cell.halfedges[0].edge.va.y);
    cell.halfedges.forEach(halfedge=>{
        ctx.lineTo(halfedge.edge.va.x, halfedge.edge.va.y);
        ctx.lineTo(halfedge.edge.vb.x, halfedge.edge.vb.y);
    });
    ctx.closePath();
    ctx.fillStyle = `#${color}`;
    ctx.strokeStyle = `#${color}`;
    ctx.stroke();
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(cell.halfedges[0].edge.vb.x, cell.halfedges[0].edge.vb.y);
    cell.halfedges.forEach(halfedge=>{
        ctx.lineTo(halfedge.edge.vb.x, halfedge.edge.vb.y);
        ctx.lineTo(halfedge.edge.va.x, halfedge.edge.va.y);
    });
    ctx.closePath();
    ctx.fillStyle = `#${color}`;
    ctx.strokeStyle = `#${color}`;
    ctx.stroke();
    ctx.fill();
}