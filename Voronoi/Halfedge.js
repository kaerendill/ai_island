class Halfedge{
    constructor(edge, l_site, r_site){
        this._site = l_site;
        this._edge = edge;

        if(r_site){
            this._angle = Math.atan2(r_site.y - l_site.y, r_site.x - l_site.x);
        }else{
            const va = edge.va;
            const vb = edge.vb;
            this._angle = edge.l_site === l_site ? Math.atan2(vb.x - va.x, va.y - vb.y) : Math.atan2(va.x - vb.x, vb.y - va.y)
        }
    }

    get site(){
        return this._site;
    }

    set site(site){
        this._site = site;
    }

    get edge(){
        return this._edge;
    }

    set edge(edge){
        this._edge = edge;
    }

    get angle(){
        return this._angle;
    }

    set angle(angle){
        this._angle = angle;
    }

    getStartPoint(){
        return this._edge.l_site === this._site ? this._edge.va : this._edge.vb;
    }

    getEndPoint(){
        return this._edge.l_site === this._site ? this._edge.vb : this._edge.va;
    }
}