class Diagram{
    constructor(cells, edges, vertices){
        this._cells = cells;
        this._edges = edges;
        this._vertices = vertices;
    }

    get cells(){
        return this._cells;
    }

    get edges(){
        return this._edges;
    }

    get vertices(){
        return this._vertices;
    }
}