class RBTree{
    constructor(root){
        this._root = root;
    }

    get root(){
        return this._root;
    }

    set root(root){
        this._root = root;
    }

    rbInsertSuccessor(node, successor){
        let parent;
        if(node){
            successor.rb_previous = node;
            successor.rb_next = node.rb_next;
            if(node.rb_next){
                node.rb_next.rb_previous = successor; 
            }
            node.rb_next = successor;
            if(node.rb_right){
                node = node.rb_right;
                while(node.rb_left){
                    node = node.rb_left;
                }
                node.rb_left = successor;
            }else{
                node.rb_right = successor;
            }
            parent = node;
        }else if(this._root){
            node = this.getFirst(this._root);

            successor.rb_previous = null;
            successor.rb_next = node;
            node.rb_previous = successor;

            node.rb_left = successor;
            parent = node;
        }else{
            successor.rb_previous  = successor.rb_next = null;

            this._root = successor;
            parent = null;
        }
        successor.rb_left = successor.rb_right = null;
        successor.rb_parent = parent;
        successor.rb_red = true;

        let grandpa;
        let uncle;
        node = successor;
        while(parent && parent.rb_red){
            grandpa = parent.rb_parent;
            if(parent == grandpa.rb_left){
                uncle = grandpa.rb_right;
                if(uncle && uncle.rb_red){
                    parent.rb_red = uncle.rb_red = false;
                    grandpa.rb_red = true;
                    node = grandpa;
                }else{
                    if(node == parent.rb_right){
                        this.rbRotateLeft(parent);
                        node = parent;
                        parent = node.rb_parent;
                    }
                    parent.rb_red = false;
                    grandpa.rb_red = true;
                    this.rbRotateRight(grandpa);
                }
            }else{
                uncle = grandpa.rb_left;
                if(uncle && uncle.rb_red){
                    parent.rb_red = uncle.rb_red = false;
                    grandpa.rb_red = true;
                    node = grandpa;
                }else{
                    if(node == parent.rb_left){
                        this.rbRotateRight(parent);
                        node = parent;
                        parent = node.rb_parent;
                    }
                    parent.rb_red = false;
                    grandpa.rb_red = true;
                    this.rbRotateLeft(grandpa);
                }
            }
            parent = node.rb_parent;
        }
        this._root.rb_red = false;
    }

    rbRemoveNode(node){
        if(node.rb_next){
            node.rb_next.rb_previous = node.rb_previous;
        }
        if(node.rb_previous){
            node.rb_previous.rb_next = node.rb_next;
        }
        node.rb_next = node.rb_previous = null;
        let parent  = node.rb_parent;
        const left = node.rb_left;
        const right = node.rb_right;
        const next = !left ? right : !right ? left : this.getFirst(right);
        if(parent){
            if(parent.rb_left === node){
                parent.rb_left = next;
            }else{
                parent.rb_right = next;
            }
        }else{
            this._root = next;
        }
        let isRed;
        if(left && right){
            isRed = next.rb_red;
            next.rb_red = node.rb_red;
            next.rb_left = left;
            left.rb_parent = next;
            if(next !== right){
                parent = next.rb_parent;
                next.rb_parent = node.rb_parent;
                node = next.rb_right;
                parent.rb_left = node;
                next.rb_right = right;
                right.rb_parent = next;
            }else{
                next.rb_parent = parent;
                parent = next;
                node = next.rb_right;
            }
        }else{
            isRed = node.rb_red;
            node = next;
        }
        if(node){
            node.rb_parent = parent;
        }
        if(isRed){
            return;
        }
        if(node && node.rb_red){
            node.rb_red = false;
            return;
        }
        let sibling;
        do{
            if(node !== this._root){
                if(node === parent.rb_left){
                    sibling = parent.rb_right;
                    if(sibling.rb_red){
                        sibling.rb_red = false;
                        parent.rb_red = true;
                        this.rbRotateLeft(parent);
                        sibling = parent.rb_right;
                    }
                    if((sibling.rb_left && sibling.rb_left.rb_red) || (sibling.rb_right && sibling.rb_right.rb_red)){
                        if(!sibling.rb_right || !sibling.rb_right.rb_red){
                            sibling.rb_left.rb_red = false;
                            sibling.rb_red = true;
                            this.rbRotateRight(sibling);
                            sibling = parent.rb_right;
                        }
                        sibling.rb_red = parent.rb_red;
                        parent.rb_red = sibling.rb_right.rb_red = false;
                        this.rbRotateLeft(parent);
                        node = this._root;
                    }
                }else{
                    sibling = parent.rb_left;
                    if(sibling.rb_red){
                        sibling.rb_red = false;
                        parent.rb_red = true;
                        this.rbRotateRight(parent);
                        sibling = parent.rb_left;
                    }
                    if((sibling.rb_left && sibling.rb_left.rb_red) || (sibling.rb_right && sibling.rb_right.rb_red)){
                        if(!sibling.rb_left || !sibling.rb_left.rb_red){
                            sibling.rb_right.rb_red = false;
                            sibling.rb_red = true;
                            this.rbRotateLeft(sibling);
                            sibling = parent.rb_left;
                        }
                        sibling.rb_red = parent.rb_red;
                        parent.rb_red = sibling.rb_left.rb_red = false;
                        this.rbRotateRight(parent);
                        node = this._root;
                    }
                }
                if(node !== this._root){
                    sibling.rb_red = true;
                    node = parent;
                    parent = parent.rb_parent;
                }
            }
        }while(node !== this._root && !node.rb_red);
        if(node){
            node.rb_red = false;
        }
    }

    rbRotateLeft(node){
        const p = node;
        const q = node.rb_right;
        const parent = p.rb_parent;
        if(parent){
            if(parent.rb_left === p){
                parent.rb_left = q;
            }else{
                parent.rb_right = q;
            }
        }else{
            this._root = q;
        }
        q.rb_parent = parent;
        p.rb_parent = q;
        p.rb_right = q.rb_left;
        if(p.rb_right){
            p.rb_right.rb_parent = p;
        }
        q.rb_left = p;
    }

    rbRotateRight(node){
        const p = node;
        const q = node.rb_left;
        const parent = p.rb_parent;
        if(parent){
            if(parent.rb_left === p){
                parent.rb_left = q;
            }else{
                parent.rb_right = q;
            }
        }else{
            this._root = q;
        }
        q.rb_parent = parent;
        p.rb_parent = q;
        p.rb_left = q.rb_right;
        if(p.rb_left){
            p.rb_left.rb_parent = p;
        }
        q.rb_right = p;
    }

    getFirst(node){
        while(node.rb_left){
            node = node.rb_left;
        }
        return node;
    }

    getLast(node){
        while(node.rb_right){
            node = node.rb_right;
        }
        return node;
    }
}