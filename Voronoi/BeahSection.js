class BeachSetion{
    constructor(site){
        this._site = site;
    }

    get site(){
        return this._site;
    }

    set site(site){
        this._site = site;
    }
}