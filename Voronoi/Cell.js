class Cell{
    constructor(site){
        this._site = site;
        this._halfedges = [];
        this._close_me = false;
        this._neighbors = [];
        this._moisure = 0;
    }

    get site(){
        return this._site;
    }

    set site(site){
        this._site = site;
    }

    get halfedges(){
        return this._halfedges;
    }

    set halfedges(halfedges){
        this._halfedges = halfedges;
    }

    get close_me(){
        return this._close_me;
    }

    set close_me(bool){
        this._close_me = bool;
    }

    get centroid(){
        return this._centroid;
    }

    set centroid(obj){
        this._centroid = obj;
    }

    get neighbors(){
        return this._neighbors;
    }

    set neighbors(obj){
        this._neighbors = obj;
    }

    get moisure(){
        return this._moisure;
    }

    set moisure(obj){
        this._moisure = obj;
    }

    prepareHalfedges(){
        const halfedges = this._halfedges;
        let i_halfedges = halfedges.length;
        let edge;

        while(i_halfedges--){
            edge = halfedges[i_halfedges].edge;
            if(!edge.vb || !edge.va){
                halfedges.splice(i_halfedges, 1);
            }
        }

        halfedges.sort((a, b)=>{
            return b.angle - a.angle;
        });
        
        return halfedges.length;
    }

    calcArea(){
        let area = 0;
        let i_halfedges = this._halfedges.length;
        while(i_halfedges--){
            const halfedge = this._halfedges[i_halfedges];
            const a = halfedge.getStartPoint();
            const b = halfedge.getEndPoint();
            area += a.x * b.y - a.y * b.x;
        }
        area /= 2;
        return area;
    }

    findCentroid(){
        let i_halfedges = this._halfedges.length;
        let x = 0;
        let y = 0;
        while(i_halfedges--){
            const halfedge = this._halfedges[i_halfedges];
            const a = halfedge.getStartPoint();
            const b = halfedge.getEndPoint();
            const v = a.x * b.y - a.y * b.x;
            x += (a.x + b.x)*v;
            y += (a.y + b.y)*v;
        }
        const d = this.calcArea() * 6;
        this._site.x = x/d;
        this._site.y = y/d;
    }
}