class Voronoi{
    constructor(sites, size, bbox){
        this._sites = sites;
        this._size = size;
        this._bbox = bbox;
        this._diagram = new Fortune().compute(sites, bbox);
    }

    get diagram(){
        return this._diagram;
    }
}