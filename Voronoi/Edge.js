class Edge{
    constructor(l_site, r_site){
        this._l_site = l_site;
        this._r_site = r_site;
        this._va = this._vb = null;
    }

    get l_site(){
        return this._l_site;
    }

    set l_site(l_site){
        this._l_site = l_site;
    }

    get r_site(){
        return this._r_site;
    }

    set r_site(r_site){
        this._r_site = r_site;
    }

    get va(){
        return this._va;
    }

    set va(va){
        this._va = va;
    }

    get vb(){
        return this._vb;
    }

    set vb(vb){
        this._vb = vb;
    }
}