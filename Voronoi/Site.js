class Site{
    constructor(x, y){
        this._voronoi_id;
        this._x = x;
        this._y = y;
        this._perlin = 0;
        this._type = "void";
    }

    get voronoi_id(){
        return this._voronoi_id;
    }

    set voronoi_id(voronoi_id){
        this._voronoi_id = voronoi_id;
    }

    get x(){
        return this._x;
    }

    set x(x){
        this._x = x;
    }

    get y(){
        return this._y;
    }

    set y(y){
        this._y = y;
    }

    get perlin(){
        return this._perlin;
    }

    set perlin(perlin){
        this._perlin = perlin;
    }

    get type(){
        return this._type;
    }

    set type(type){
        this._type = type;
    }
}