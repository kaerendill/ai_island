class Fortune{
    constructor(){
        this._vertices = [];
        this._edges = [];
        this._cells = [];
        this._beachline = new RBTree();
        this._circle_events = new RBTree();
        this._to_recycle = null;
        this._beach_section_junkyard = [];
        this._circle_event_junkyard = [];
        this._vertex_junkyard = [];
        this._edge_junkyard = [];
        this._cell_junkyard = [];

        //Maths
        this._e = 1e-9;
        this._inve = 1/this._e;
        this._nb_lloyd = 5;
    }

    reset(){
        this._vertices = [];
        this._edges = [];
        this._cells = [];
        this._beachline = new RBTree();
        this._circle_events = new RBTree();
        this._to_recycle = null;
        this._beach_section_junkyard = [];
        this._circle_event_junkyard = [];
        this._vertex_junkyard = [];
        this._edge_junkyard = [];
        this._cell_junkyard = [];
    }

    equalWithEpsilon(a, b){
        return Math.abs(a - b) < this._e;
    }

    greaterThanWithEpsilon(a, b){
        return a - b > this._e;
    }

    greaterThanOrWithEpsilon(a, b){
        return b - a < this._e;
    }

    lessThanWithEpsilon(a, b){
        return b - a > this._e;
    }

    lessThanOrWithEpsilon(a, b){
        return a - b < this._e;
    }

    createCell(site){
        const cell = this._cell_junkyard.pop();
        if(cell){
            return cell.site = site;
        }else{
            return new Cell(site);
        }
    }

    createVertex(x, y){
        let v = this._vertex_junkyard.pop();
        if(!v){
            v = new Vertex(x, y);
        }else{
            v.x = x;
            v.y = y;
        }
        this._vertices.push(v);
        return v;
    }

    createEdge(l_site, r_site, va, vb){
        let edge = this._edge_junkyard.pop();
        if(!edge){
            edge = new Edge(l_site, r_site);
        }else{
            edge.l_site = l_site;
            edge.r_site = r_site;
            edge.va = edge.vb = null;
        }
        this._edges.push(edge);
        if(va){
            this.setEdgeStartPoint(edge, l_site, r_site, va);
        }
        if(vb){
            this.setEdgeEndPoint(edge, l_site, r_site, vb);
        }
        this._cells[l_site.voronoi_id].halfedges.push(this.createHalfedge(edge, l_site, r_site));
        this._cells[r_site.voronoi_id].halfedges.push(this.createHalfedge(edge, r_site, l_site));
        this._cells[l_site.voronoi_id].neighbors.push(this._cells[r_site.voronoi_id]);
        this._cells[r_site.voronoi_id].neighbors.push(this._cells[l_site.voronoi_id]);
        return edge;
    }

    createHalfedge(edge, l_site, r_site){
        return new Halfedge(edge, l_site, r_site);
    }

    createBorderEdge(l_site, va, vb){
        let edge = this._edge_junkyard.pop();
        if(!edge){
            edge = new Edge(l_site, null);
        }else{
            edge.l_site = l_site;
            edge.r_site = null;
        }
        edge.va = va;
        edge.vb = vb;
        this._edges.push(edge);
        return edge;
    }

    setEdgeStartPoint(edge, l_site, r_site, vertex){
        if(!edge){
            console.log("ok");
        }
        if(!edge.va && !edge.vb){
            edge.va = vertex;
            edge.l_site = l_site;
            edge.r_site = r_site;
        }else if(edge.l_site === r_site) {
            edge.vb = vertex;
        }else{
            edge.va = vertex;
        }
    }

    setEdgeEndPoint(edge, l_site, r_site, vertex){
        this.setEdgeStartPoint(edge, r_site, l_site, vertex);
    }

    createBeachSection(site){
        let beach_section = this._beach_section_junkyard.pop();
        if(!beach_section){
            beach_section = new BeachSetion(site);
        }else{
            beach_section.site = site;
        }
        return beach_section;
    }

    leftBreakPoint(arc, directrix){
        let site = arc.site;
        const r_foc_x = site.x;
        const r_foc_y = site.y;
        const pby2 = r_foc_y - directrix;
        if(!pby2){
            return r_foc_x;
        }
        const l_arc = arc.rb_previous;
        if(!l_arc){
            return -Infinity;
        }
        site = l_arc.site;
        const l_foc_x = site.x;
        const l_foc_y = site.y;
        const plby2 = l_foc_y - directrix;
        if(!plby2){
            return l_foc_x;
        }
        const hl = l_foc_x - r_foc_x;
        const aby2 = 1/pby2 - 1/plby2;
        const b = hl/plby2;
        if(aby2){
            return (-b+Math.sqrt(b**2-2*aby2*(hl**2/(-2*plby2)-l_foc_y+plby2/2+r_foc_y-pby2/2)))/aby2+r_foc_x;
        }
        return (r_foc_x+l_foc_x)/2;
    }

    rightBreakPoint(arc, directrix){
        const r_arc = arc.rb_next;
        if(r_arc){
            return this.leftBreakPoint(r_arc, directrix);
        }
        const site = arc.site;
        return site.y === directrix ? site.x : Infinity;
    }

    detachBeachSection(beach_section){
        this.detachCircleEvent(beach_section);
        this._beachline.rbRemoveNode(beach_section);
        this._beach_section_junkyard.push(beach_section);
    }

    removeBeachSection(beach_section){
        const circle = beach_section.circle_event;
        const x = circle.x;
        const y = circle.y_center;
        const vertex = this.createVertex(x, y);
        let previous = beach_section.rb_previous;
        let next = beach_section.rb_next;
        const disappearingTransitions = [beach_section];

        this.detachBeachSection(beach_section);

        let l_arc = previous;
        while(l_arc.circle_event && Math.abs(x - l_arc.circle_event.x)<this._e && Math.abs(y - l_arc.circle_event.y_center)<this._e){
            previous = l_arc.rb_previous;
            disappearingTransitions.unshift(l_arc);
            this.detachBeachSection(l_arc);
            l_arc = previous;
        }
        disappearingTransitions.unshift(l_arc);
        this.detachCircleEvent(l_arc);

        let r_arc = next;
        while(r_arc.circle_event && Math.abs(x-r_arc.circle_event.x)<this._e && Math.abs(y-r_arc.circle_event.y_center)<this._e){
            next = r_arc.rb_next;
            disappearingTransitions.push(r_arc);
            this.detachBeachSection(r_arc);
            r_arc = next;
        }
        disappearingTransitions.push(r_arc);
        this.detachCircleEvent(r_arc);

        const n_arcs = disappearingTransitions.length;
        for(let i_arc = 1; i_arc < n_arcs; i_arc++){
            r_arc = disappearingTransitions[i_arc];
            l_arc = disappearingTransitions[i_arc-1];
            this.setEdgeStartPoint(r_arc.edge, l_arc.site, r_arc.site, vertex);
        }

        l_arc = disappearingTransitions[0];
        r_arc = disappearingTransitions[n_arcs-1];
        r_arc.edge = this.createEdge(l_arc.site, r_arc.site, undefined, vertex);

        this.attachCircleEvent(l_arc);
        this.attachCircleEvent(r_arc);
    }

    addBeachSection(site){
        const x = site.x;
        const directrix = site.y;

        let l_arc;
        let r_arc;
        let l_dx;
        let r_dx;
        let node = this._beachline.root;
        let force_end = false;

        while(node && l_arc != node && !force_end){
            l_dx = this.leftBreakPoint(node, directrix)-x;
            if(l_dx > this._e){
                node = node.rb_left;
            }else{
                r_dx = x-this.rightBreakPoint(node, directrix);
                if(r_dx > -this._e){
                    if(!node.rb_right){
                        l_arc = node;
                    }else{
                        node = node.rb_right;
                    }
                }else{
                    if(l_dx > -this._e){
                        l_arc = node.rb_previous;
                        r_arc = node;   
                    }else if(r_dx > -this._e){
                        l_arc = node;
                        r_arc = node.rb_next;
                    }else{
                        l_arc = r_arc = node;
                    }
                    force_end = true;
                }
            }
        }

        const new_arc = this.createBeachSection(site);
        this._beachline.rbInsertSuccessor(l_arc, new_arc);

        if(!l_arc && !r_arc){
        }else if(l_arc === r_arc){
            this.detachCircleEvent(l_arc);

            r_arc = this.createBeachSection(l_arc.site);
            this._beachline.rbInsertSuccessor(new_arc, r_arc);
            new_arc.edge = r_arc.edge = this.createEdge(l_arc.site, new_arc.site);

            this.attachCircleEvent(l_arc);
            this.attachCircleEvent(r_arc);
        }else if(l_arc && !r_arc){
            new_arc.edge = this.createEdge(l_arc.site, new_arc.site);
        }else if(l_arc !== r_arc){
            this.detachCircleEvent(l_arc);
            this.detachCircleEvent(r_arc);

            const l_site = l_arc.site;
            const ax = l_site.x;
            const ay = l_site.y;
            const bx = site.x - ax;
            const by = site.y - ay;
            const r_site = r_arc.site;
            const cx = r_site.x - ax;
            const cy = r_site.y - ay;
            const d = 2*(bx*cy - by*cx);
            const hb = bx**2 + by**2;
            const hc = cx**2 + cy**2;
            const vertex  = this.createVertex((cy*hb-by*hc)/d+ax, (bx*hc-cx*hb)/d+ay);

            this.setEdgeStartPoint(r_arc.edge, l_site, r_site, vertex);

            new_arc.edge = this.createEdge(l_site, site, undefined, vertex);
            r_arc.edge = this.createEdge(site, r_site, undefined, vertex);

            this.attachCircleEvent(l_arc);
            this.attachCircleEvent(r_arc);
        }
    }

    attachCircleEvent(arc){
        const l_arc = arc.rb_previous;
        const r_arc = arc.rb_next;
        if(!l_arc || !r_arc){
            return;
        }
        const l_site = l_arc.site;
        const c_site = arc.site;
        const r_site = r_arc.site;
        if(l_site !== r_site){
            const bx = c_site.x;
            const by = c_site.y;
            const ax = l_site.x - bx;
            const ay = l_site.y - by;
            const cx = r_site.x - bx;
            const cy = r_site.y - by;
            const d = 2*(ax*cy-ay*cx);
            if(d >= -2e-12){
                return;
            }
            const ha = ax**2 + ay**2;
            const hc = cx**2 + cy**2;
            const x = (cy * ha - ay * hc)/d;
            const y = (ax * hc - cx * ha)/d;
            const y_center = y + by;
            let circle_event = this._circle_event_junkyard.pop();
            if(!circle_event){
                circle_event = new CircleEvent();
            }
            circle_event.arc = arc;
            circle_event.site = c_site;
            circle_event.x = x+bx;
            circle_event.y = y_center+Math.sqrt(x**2+y**2);
            circle_event.y_center = y_center;
            arc.circle_event = circle_event;
            let predecessor = null;
            let node = this._circle_events.root;
            let force_end = false;
            while(node && !force_end){
                if(circle_event.y < node.y || (circle_event.y === node.y && circle_event.x <= node.x)){
                    if(node.rb_left){
                        node = node.rb_left;
                    }else{
                        predecessor = node.rb_previous;
                        force_end = true;
                    }
                }else{
                    if(node.rb_right){
                        node = node.rb_right;
                    }else{
                        predecessor = node;
                        force_end = true;
                    }
                }
            }
            this._circle_events.rbInsertSuccessor(predecessor, circle_event);
            if(!predecessor){
                this._first_circle_event = circle_event;
            }
        }
    }

    detachCircleEvent(arc){
        const circle_event = arc.circle_event;
        if(circle_event){
            if(!circle_event.rb_previous){
                this._first_circle_event = circle_event.rb_next;
            }
            this._circle_events.rbRemoveNode(circle_event);
            this._circle_event_junkyard.push(circle_event);
            arc.circle_event = null;
        }
    }

    connectEdge(edge, bbox){
        let vb = edge.vb;
        if(vb){
            return true;
        }

        let va = edge.va;
        const xl = bbox.xl;
        const xr = bbox.xr;
        const yt = bbox.yt;
        const yb = bbox.yb;
        const l_site = edge.l_site;
        const r_site = edge.r_site;
        const lx = l_site.x;
        const ly = l_site.y;
        const rx = r_site.x;
        const ry = r_site.y;
        const fx = (lx+rx)/2;
        const fy = (ly+ry)/2;
        let fm;
        let fb;

        this._cells[l_site.voronoi_id].close_me = true;
        this._cells[r_site.voronoi_id].close_me = true;

        if(ry !== ly){
            fm = (lx-rx)/(ry-ly);
            fb = fy-fm*fx;
        }

        if(fm === undefined){
            if(fx < xl || fx >= xr){
                return false;
            }
            if(lx > rx){
                if(!va || va.y < yt){
                    va = this.createVertex(fx, yt);
                }else if(va.y >= yb){
                    return false;
                }
                vb = this.createVertex(fx, yb);
            }else{
                if(!va || va.y > yb){
                    va = this.createVertex(fx, yb);
                }else if(va.y < yt){
                    return false;
                }
                vb = this.createVertex(fx, yt);
            }
        }else if(fm < -1 || fm > 1){
            if(lx > rx){
                if(!va || va.y < yt){
                    va = this.createVertex((yt-fb)/fm, yt);
                }else if(va.y >= yb){
                    return false;
                }
                vb = this.createVertex((yb-fb)/fm, yb);
            }else{
                if(!va || va.y > yb){
                    va = this.createVertex((yb-fb)/fm, yb);
                }else if(va.y < yt){
                    return false;
                }
                vb = this.createVertex((yt-fb)/fm, yt);
            }
        }else{
            if(ly < ry){
                if(!va || va.x < xl){
                    va = this.createVertex(xl, fm*xl+fb);
                }else if(va.x >= xr){
                    return false;
                }
                vb = this.createVertex(xr, fm*xr+fb);
            }else{
                if(!va || va.x > xr){
                    va = this.createVertex(xr, fm*xr+fb);
                }else if(va.x < xl){
                    return false;
                }
                vb = this.createVertex(xl, fm*xl+fb);
            }
        }
        edge.va = va;
        edge.vb = vb;
        return true;
    }

    clipEdge(edge, bbox){
        const ax = edge.va.x;
        const ay = edge.va.y;
        const bx = edge.vb.x;
        const by = edge.vb.y;
        let t0 = 0;
        let t1 = 1;
        const dx = bx - ax;
        const dy = by - ay;

        let q = ax-bbox.xl;
        if(dx === 0 && q<0){
            return false;
        }
        let r = -q/dx;
        if(dx < 0){
            if(r < t0){
                return false;
            }
            if(r < t1){
                t1 = r;
            }
        }else if(dx > 0){
            if(r>t1){
                return false;
            }
            if(r>t0){
                t0 = r;
            }
        }

        q = bbox.xr-ax;
        if(dx === 0 && q<0){
            return false;
        }
        r = q/dx;
        if(dx<0){
            if(r>t1){
                return false;
            }
            if(r>t0){
                t0 = r;
            }
        }else if(dx>0){
            if(r<t0){
                return false;
            }
            if(r<t1){
                t1 = r;
            }
        }

        q = ay-bbox.yt;
        if(dy === 0 && q<0){
            return false;
        }
        r = -q/dy;
        if(dy<0){
            if(r<t0){
                return false;
            }
            if(r<t1){
                t1 = r;
            }
        }else if(dy > 0){
            if(r>t1){
                return false;
            }
            if(r>t0){
                t0 = r;
            }
        }

        q = bbox.yb-ay;
        if(dy === 0 && q<0){
            return false;
        }
        r = q/dy;
        if(dy < 0){
            if(r > t1){
                return false;
            }
            if(r > t0){
                t0 =r;
            }
        }else if(dy > 0){
            if(r < t0){
                return false;
            }
            if(r < t1){
                t1 = r;
            }
        }

        if(t0 > 0){
            edge.va = this.createVertex(ax+t0*dx, ay+t0*dy);
        }
        if(t1 < 1){
            edge.vb = this.createVertex(ax+t1*dx, ay+t1*dy);
        }
        if(t0 > 0 || t1 < 1){
            this._cells[edge.l_site.voronoi_id].close_me = true;
            this._cells[edge.r_site.voronoi_id].close_me = true;
        }
        return true;
    }

    clipEdges(bbox){
        const edges = this._edges;
        let i_edge = edges.length;
        let edge;

        while(i_edge--){
            edge = edges[i_edge];
            if(!this.connectEdge(edge, bbox) || !this.clipEdge(edge, bbox) || (Math.abs(edge.va.x - edge.vb.x)<this._e && Math.abs(edge.va.y - edge.vb.y) < this._e)){
                edge.va = edge.vb = null;
                edges.splice(i_edge, 1);
            }
        }
    }

    closeCells(bbox){
        const xl = bbox.xl;
        const xr = bbox.xr;
        const yt = bbox.yt;
        const yb = bbox.yb;
        const cells = this._cells;
        let i_cell = cells.length;
        let cell;
        let i_left;
        let halfedges;
        let n_halfedges;
        let edge;
        let va;
        let vb;
        let vz;
        let last_border_segment;

        while(i_cell--){
            cell = cells[i_cell];
            if(!cell.prepareHalfedges()){
                continue;
            }
            if(!cell.close_me){
                continue;
            }
            halfedges = cell.halfedges;
            n_halfedges = halfedges.length;

            i_left = 0;
            while(i_left < n_halfedges){
                va = halfedges[i_left].getEndPoint();
                vz = halfedges[(i_left+1)%n_halfedges].getStartPoint();
                if(Math.abs(va.x-vz.x)>=this._e || Math.abs(va.y-vz.y)>=this._e){
                    switch (true){
                        case this.equalWithEpsilon(va.x, xl) && this.lessThanWithEpsilon(va.y, yb):
                            last_border_segment = this.equalWithEpsilon(vz.x, xl);
                            vb = this.createVertex(xl, last_border_segment ? vz.y : yb);
                            edge = this.createBorderEdge(cell.site, va, vb);
                            i_left++;
                            halfedges.splice(i_left, 0, this.createHalfedge(edge, cell.site, null));
                            n_halfedges++;
                            if(last_border_segment){
                                break;
                            }
                            va = vb;
                        
                        case this.equalWithEpsilon(va.y, yb) && this.lessThanWithEpsilon(va.x, xr):
                            last_border_segment = this.equalWithEpsilon(vz.y, yb);
                            vb = this.createVertex(last_border_segment ? vz.x : xr, yb);
                            edge = this.createBorderEdge(cell.site, va, vb);
                            i_left++;
                            halfedges.splice(i_left, 0, this.createHalfedge(edge, cell.site, null));
                            n_halfedges++;
                            if(last_border_segment){
                                break;
                            }
                            va = vb;

                        case this.equalWithEpsilon(va.x, xr) && this.greaterThanWithEpsilon(va.y, yt):
                            last_border_segment = this.equalWithEpsilon(vz.x, xr);
                            vb = this.createVertex(xr, last_border_segment ? vz.y : yt);
                            edge = this.createBorderEdge(cell.site, va, vb);
                            i_left++;
                            halfedges.splice(i_left, 0, this.createHalfedge(edge, cell.site, null));
                            n_halfedges++;
                            if(last_border_segment){
                                break;
                            }
                            va = vb;
                        
                        case this.equalWithEpsilon(va.y, yt) && this.greaterThanWithEpsilon(va.x, xl):
                            last_border_segment = this.equalWithEpsilon(vz.y, yt);
                            vb = this.createVertex(last_border_segment ? vz.x : xl, yt);
                            edge = this.createBorderEdge(cell.site, va, vb);
                            i_left++;
                            halfedges.splice(i_left, 0, this.createHalfedge(edge, cell.site, null));
                            n_halfedges++;
                            if(last_border_segment){
                                break;
                            }
                            va = vb;
    
                            last_border_segment = this.equalWithEpsilon(vz.x, xl);
                            vb = this.createVertex(xl, last_border_segment ? vz.y : yb);
                            edge = this.createBorderEdge(cell.site, va, vb);
                            i_left++;
                            halfedges.splice(i_left, 0, this.createHalfedge(edge, cell.site, null));
                            n_halfedges++;
                            if(last_border_segment){
                                break;
                            }
                            va = vb;
    
                            last_border_segment = this.equalWithEpsilon(vz.y, yb);
                            vb = this.createVertex(last_border_segment ? vz.x : xr, yb);
                            edge = this.createBorderEdge(cell.site, va, vb);
                            i_left++;
                            halfedges.splice(i_left, 0, this.createHalfedge(edge, cell.site, null));
                            n_halfedges++;
                            if(last_border_segment){
                                break;
                            }
                            va = vb;
    
                            last_border_segment = this.equalWithEpsilon(vz.x, xr);
                            vb = this.createVertex(xr, last_border_segment ? vz.y : yt);
                            edge = this.createBorderEdge(cell.site, va, vb);
                            i_left++;
                            halfedges.splice(i_left, 0, this.createHalfedge(edge, cell.site, null));
                            n_halfedges++;
                            if(last_border_segment){
                                break;
                            }
                    }
                }
                i_left++;
            }
            cell.close_me = false;
        }
    }

    compute(sites, bbox){
        const site_events = sites.slice(0);
        site_events.sort((a,b)=>{ 
            return b.y - a.y ? b.y - a.y : b.x - a.x;
        });
        let site = site_events.pop();
        let site_id = 0;
        let x_site_x;
        let x_site_y;
        const cells = this._cells;
        let circle;
        let end = false;
        while(!end){
            circle = this._first_circle_event;

            if(site && (!circle || site.y < circle.y || (site.y === circle.y && site.x < circle.x))){
                if(site.x !== x_site_x || site.y !== x_site_y){
                    cells[site_id] = this.createCell(site);
                    site.voronoi_id = site_id++;
                    this.addBeachSection(site);
                    x_site_x = site.x;
                    x_site_y = site.y;
                }
                site = site_events.pop();
            }else if(circle){
                this.removeBeachSection(circle.arc);
            }else{
                end = true;
            }
        }

        this.clipEdges(bbox);
        this.closeCells(bbox);
        if(this._nb_lloyd){
            this._nb_lloyd--;
            const sites = [];
            this._cells.forEach(cell=>{
                cell.findCentroid();
                sites.push(new Site(cell.site.x, cell.site.y));
            });
            this.reset();
            this.compute(sites, bbox);
        } 

        return new Diagram(cells, this._edges, this._vertices);
    }
}