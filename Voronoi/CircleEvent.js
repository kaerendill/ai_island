class CircleEvent{
    constructor(arc, site, x, y, y_center){
        this._arc = arc;
        this._rb_left;
        this._rb_right;
        this._rb_next;
        this._rb_parent;
        this._rb_previous;
        this._rb_red;
        this._site =site;
        this._x = x;
        this._y = y;
        this._y_center = y_center;
    }

    get arc(){
        return this._arc;
    }

    set arc(arc){
        this._arc = arc;
    }

    get rb_left(){
        return this._rb_left;
    }

    set rb_left(rb_left){
        this._rb_left = rb_left;
    }

    get rb_right(){
        return this._rb_right;
    }

    set rb_right(rb_right){
        this._rb_right = rb_right;
    }

    get rb_parent(){
        return this._rb_parent;
    }

    set rb_parent(rb_parent){
        this._rb_parent = rb_parent;
    }

    get rb_previous(){
        return this._rb_previous;
    }

    set rb_previous(rb_previous){
        this._rb_previous = rb_previous;
    }

    get rb_red(){
        return this._rb_red;
    }

    set rb_red(rb_red){
        this._rb_red = rb_red;
    }

    get rb_next(){
        return this._rb_next;
    }

    set rb_next(rb_next){
        this._rb_next = rb_next;
    }

    get site(){
        return this._site;
    }

    set site(site){
        this._site = site;
    }

    get x(){
        return this._x;
    }

    set x(x){
        this._x = x;
    }

    get y(){
        return this._y;
    }

    set y(y){
        this._y = y;
    }
    get y_center(){
        return this._y_center;
    }

    set y_center(y_center){
        this._y_center = y_center;
    }
}